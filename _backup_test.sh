#!/bin/bash
# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : Создает резервную копию ИБ
# Синтаксис  : 

# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

BASENAME=test
BACKUPDIR=/mnt/backup/$BASENAME # Директория хранения бэкапов
DATENAME=`date +%Y%m%d%H%M` # Дата/Время в имени файла бэкапа
#LOGFILE=/var/log/1stools.log # Хранилище лог-файлов
IBPASS=$(echo cGFzc3dvcmQK|base64 -d) # Пароль ИБ. Для генерации хеша воспользуйтесь ./passdecode.sh

mkdir -p $BACKUPDIR

slog "Резервное копирование dt ИБ $BASENAME запущено"
$spath/ibcmd infobase dump --db-server=localhost --dbms=postgresql --db-name=$BASENAME --db-user=postgres --db-pwd=$SQL_P $BACKUPDIR/$DATENAME.dt
# --user=Администратор --password="$IBPASS"
slog "Резервное копирование dt ИБ $BASENAME завершено"
