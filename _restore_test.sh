#!/bin/bash
# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : Восстанавливает ИБ из последней резервной копии
# Синтаксис  : 

# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

BASENAME=test
BACKUPDIR=/mnt/backup/$BASENAME # Директория хранения бэкапов
DATENAME=`date +%Y%m%d%H%M` # Дата/Время в имени файла бэкапа
IBPASS=$(echo cGFzc3dvcmQK|base64 -d) # Пароль ИБ. Для генерации хеша воспользуйтесь ./passdecode.sh


for i in $(ls -tr $BACKUPDIR/*.dt);
do
  ARH_FILE=$i; #последний файл
done
#ARH_FILE=/mnt/backup/1S/base/dt/umkp/202312120202.dt

slog "Загрузка $BASENAME из $ARH_FILE запущено"

$spath/ibcmd infobase restore --db-server=localhost --dbms=postgresql --db-name=$BASENAME --db-user=postgres --db-pwd=$SQL_P $ARH_FILE
# --user=Администратор --password=$IBPASS

slog "Загрузка $BASENAME из $ARH_FILE завершено"
