#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : Изменяет "зашифрованный" пассворд к серверу БД в файле config.sh
# Синтаксис  : setsqlsec.sh [Имя пользователя] [пароль]


# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if ! [ -f "$CDIR/config.sh" ]; then
    echo "Файл конфигурации $CDIR/config.bkp не существует."
    source $CDIR/createconfig.sh
fi

sql_user=$1
sql_pass=$2


if [ ${#sql_user} -lt 2 ];
then
    echo "Введите логин SQL пользователя:"
    read Str
else
    Str=$sql_user
fi
EncodedStr=$(echo $Str|base64)
cp $CDIR/config.sh $CDIR/config.bkp
cat config.bkp|sed /SQL_U=/c"SQL_U=\$(echo $EncodedStr|base64 -d)" > $CDIR/config.sh
rm $CDIR/config.bkp


if [ ${#sql_pass} -lt 2 ];
then
    echo "Введите пароль SQL пользователя:"
    read Str
else
    Str=$sql_pass
fi
EncodedStr=$(echo $Str|base64)
#NewStr="SQL_P=\$(echo $EncodedStr|base64 -d)"
cp $CDIR/config.sh $CDIR/config.bkp
cat config.bkp|sed /SQL_P=/c"SQL_P=\$(echo $EncodedStr|base64 -d)" > $CDIR/config.sh
rm $CDIR/config.bkp
