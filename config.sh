#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : Устанавливает необходимые переменные
# Синтаксис  : source $PWD/config.sh
#              config.sh show - показ переменных

# путь к серверу 1с
spath=$(ps -axu | grep rphost | grep -v grep | awk '{print $11}')
spath=$(echo $spath | sed -e "s/\/rphost//")

LOGFILE=/var/log/1stools.log

#Имя пользователя для доступа к SMB шаре
SMB_U=$(echo c21idXNlcgo=|base64 -d)

#Пассворд для доступа к SMB шаре
SMB_P=$(echo c21icGFzc3dvcmQK|base64 -d)


#Имя пользователя  SQL сервера
SQL_U=$(echo cG9zdGdyZXMK|base64 -d)

#Пассворд SQL сервера
SQL_P=$(echo c2Ftb2xldAo=|base64 -d)

if [ ! $1 == "" ] && [ $1 == "show" ]; 
then 
	echo "spath	: $spath"
	echo "SMB_U	: $SMB_U"
	echo "SMB_P	: $SMB_P"
	echo "SQL_U	: $SQL_U"
	echo "SQL_P	: $SQL_P"
fi

slog () {
    echo "`date +%Y.%m.%d-%H:%M:%S` $1" >> $LOGFILE
    logger "`date +%Y.%m.%d-%H:%M:%S` $1"
}
