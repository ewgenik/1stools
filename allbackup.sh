#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : Запускает все скрипты для резервного копирования по маске backup_*.sh. За тем аналогично запускает скрипты на восстановление
# Синтаксис  :

for bkp in `find /home/1stools -type f -name "backup_*.sh"`
do
    . $bkp;
done

for rst in `find /home/1stools -type f -name "restore_*.sh"`
do
    . $rst;
done
