#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : 	Возвращает список баз данных с объемом. 
# Синтаксис  : 	./pggetbasesizelist.sh


# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

PGPASSWORD="$SQL_P" psql --host localhost --username postgres --command 'SELECT pg_database.datname, pg_database_size(pg_database.datname) AS size, pg_size_pretty(pg_database_size(pg_database.datname)) FROM pg_database ORDER by size;'


