#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : 	Тест производительности PostgreSQL сервера. 
# Синтаксис  : 	.\pgbenchmark.sh


# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

# создаем базу данных
PGPASSWORD="$SQL_P" psql --host localhost --username postgres --command 'SHOW ALL;'


