#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : 	Удаляет 1С базу данных на localhost 1С сервере. 
# Синтаксис  : 	.\drop1sbase.sh 1sbaseuuid
# 		1sbaseuuid - UUID базы 1С


#название базы 1С
baseuuid=$1
#название SQL базы данных

# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

#Настройки сервера
$spath/ras --daemon cluster
clusterID=$($spath/rac cluster list|grep cluster|sed 's/cluster                       : //')
echo clusterID:$clusterID

# IP адрес сервера
serverIP=$(hostname -I)
echo serverIP:$serverIP

# Строка состоящая из даты и времени для резервных копий конфигов
DATENAME=`date +%Y%m%d%H%M`

# проверяем введено ли название базы в качестве параметра 1
if [ ${#baseuuid} -gt 35 ]; 
then 
	echo "1SBaseUUID:$baseuuid";

read -t 15 -N 1 -p "Удалить базу $servers. (y/N)? " answer
echo
if [ "${answer,,}" == "y" ]
then
    echo "Удаляем базу uuid:$baseuuid"
	# Удаляем базу 1С
#	$spath/rac infobase --cluster=$clusterID drop --infobase=$baseuuid --drop-database
	$spath/rac infobase --cluster=$clusterID drop --infobase=$baseuuid
fi


else
	echo "Использование: $0 1sbaseuuid"
	echo ""
	echo "Удаляет 1С базу данных на localhost 1С сервере."
	echo ""
	echo "Аргументы:"
	echo "1sbaseuuid - UUID базы 1С (минимум 36 символов)"
	echo "UUID базы можно узнать командой ./getbaselist.sh"


fi
