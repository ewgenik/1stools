#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : Возвращает список сессий к серверу 1с
# Синтаксис  : 

# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

$spath/ras --daemon cluster
clusterID=$($spath/rac cluster list|grep cluster|sed 's/cluster                       : //')
#echo clusterID:$clusterID

$spath/rac session --cluster=$clusterID list | grep "host\|started-at\|last-active-at"
