#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : Загружает тест Гилева в существующую информационную базу
# Синтаксис  :

# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

BASENAME=gilev
ARH_FILE=/tmp/GILV_TPC_G1C_832.dt

# Проверка существования DT теста Гилева с загрузкой по необходимости
if [ ! -f "$ARH_FILE" ]; then
    wget http://files.garantsoft.com/GILV_TPC_G1C_83.dt --output-document=$ARH_FILE
fi

# Проверка существования DT теста Гилева
if [ ! -f "$ARH_FILE" ]; then
        echo "Файл $ARH_FILE не существует."
        exit 0
fi

slog "Загрузка $BASENAME из $ARH_FILE запущено"
$spath/ibcmd infobase restore --db-server=localhost --dbms=postgresql --db-name=$BASENAME --db-user=postgres --db-pwd=$SQL_P $ARH_FILE # --user=Администратор --password=$IBPASS
slog "Загрузка $BASENAME из $ARH_FILE завершено"
