#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : Возвращает версию 1с сервера.
# Синтаксис  : getplatformversion.sh

# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

$spath/rac -v
