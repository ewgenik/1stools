#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : Удаление сессий пользователя 1С
# Синтаксис  : terminateusersession.sh

# Имя пользователя
username='Никоноров Евгений'

# UUID информационной базы
infobase=f79524c7-3632-4ab8-b8c9-ffe34c7212b4

# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

mkdir -p /var/log/1stools
log="/var/log/1stools/termusersession.log"
tmp="/tmp/termusersession.tmp"
rm $tmp
echo "$(date +"%F %T") Begin" >> $log

$spath/ras --daemon cluster
clusterID=$($spath/rac cluster list|grep cluster|sed 's/cluster                       : //')
echo "$(date +"%F %T") clusterID : $clusterID" >> $log

$spath/rac session --cluster=$clusterID list | grep "session                          :" | awk '{print $3}' | \

while read sessionID; do
    $spath/rac session --cluster=$clusterID info --session=$sessionID | \
    while read param1 param2 param3; do
    if [ "$param1" = "session" ] || [ "$param1" = "user-name" ] || [ "$param1" = "infobase" ]
    then
        echo -n "$param3 " >> $tmp;
    fi
    done;
    echo "" >> $tmp;
done
echo "$(date +"%F %T") pre session list :" >> $log
cat $tmp >> $log


cat $tmp | \
while read param1 param2 param3; do
    if [[ "$param3" == "$username" ]] && [[ "$param2" == "$infobase" ]]
    then
    echo "$(date +"%F %T") terminate session $param1 ($param3)" >> $log
## Внимание! Эта строка удаляет сессии.
#   $spath/rac session --cluster=$clusterID terminate --session=$param1
    fi
done

echo "$(date +"%F %T") End" >> $log
echo "" >> $log
