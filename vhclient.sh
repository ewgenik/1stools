#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение :  Запуск клиента virtualhere.
# Синтаксис  :  ./vhclient.sh

BIN_PATH=/usr/bin/vhclientx86_64
#BIN_PATH=/opt/virtualhere/vhclientx86_64
SVC_PATH=/etc/systemd/system/virtualhereclientgarant.service

if [ ! -f "$BIN_PATH" ]; then
    wget http://files.garantsoft.com/vhclientx86_64 --output-document=$BIN_PATH
    chmod +x $BIN_PATH
    wget http://files.garantsoft.com/virtualhereclientgarant.service --output-document=$SVC_PATH
    systemctl daemon-reload
    systemctl enable virtualhereclientgarant.service
    echo "run
systemctl start virtualhereclientgarant.service"
else

    $BIN_PATH -n
    $BIN_PATH -t "USE,hardhasp.71"
    $BIN_PATH -t "LIST"
fi


