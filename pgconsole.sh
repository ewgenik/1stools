#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение :  Тест производительности PostgreSQL сервера.
# Синтаксис  :  .\pgconsole.sh


# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

# Запускаем консоль PGSQL
echo $SQL_P
PGPASSWORD="$SQL_P" psql --host localhost --username postgres
