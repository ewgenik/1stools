#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : 	Тест производительности PostgreSQL сервера. 
# Синтаксис  : 	.\pgbenchmark.sh


# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

# создаем базу данных
#PGPASSWORD="$SQL_P" psql --host localhost --username postgres --command "CREATE DATABASE benchmark;"
PGPASSWORD="$SQL_P" createdb --host localhost --username "postgres" "benchmark" --no-password

# Инициализируем данные для теста
PGPASSWORD="$SQL_P" pgbench -h localhost -U "postgres" -i -s 18 benchmark 

# Непосредственно тест производительности
PGPASSWORD="$SQL_P" pgbench -h localhost -U "postgres" -c 16 -j 16 -T 60 -r benchmark

# Удаляем базу данных
PGPASSWORD="$SQL_P" dropdb --host localhost --username "postgres" --no-password "benchmark" --if-exists


