#!/bin/bash 
basename=$1
# IP адрес сервера
serverIP=$(hostname -I)
echo serverIP:$serverIP

# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

echo $spath
echo $basename
echo $serverIP

if [ ${#basename} -gt 3 ]; 
then
	echo "BaseName=$basename";
	# создаем файл конфигурации для вебсервера
	touch /etc/apache2/sites-enabled/$basename.conf
	# создаем директорию для публикации базы на вебсервере
	mkdir /var/www/$basename
	# публикуем базу на вебсервере
	cd $spath
	./webinst -apache24 -wsdir $basename -dir /var/www/$basename -connstr "Srvr=localhost;Ref=$basename;" -confPath /etc/apache2/sites-enabled/$basename.conf
	service apache2 reload
else
	echo "Short base name" ; 
	exit 
fi
