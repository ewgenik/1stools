#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : Удаление старых сессий
# Синтаксис  : terminatesession.sh

# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

mkdir -p /var/log/1stools
log="/var/log/1stools/termsession.log"
tmp="/tmp/termsession.tmp"
rm $tmp
echo "$(date +"%F %T") Begin" >> $log

#/opt/1C/v8.3/x86_64/ras --daemon cluster
#clusterID=$(/opt/1C/v8.3/x86_64/rac cluster list|grep cluster|sed 's/cluster                       : //')
$spath/ras --daemon cluster
clusterID=$($spath/rac cluster list|grep cluster|sed 's/cluster                       : //')
echo "$(date +"%F %T") clusterID : $clusterID" >> $log

#cd /opt/1C/v8.3/x86_64/
#./rac session --cluster=$clusterID list | grep "session                          :" | awk '{print $3}' | \
$spath/rac session --cluster=$clusterID list | grep "session                          :" | awk '{print $3}' | \

while read sessionID; do 
    $spath/rac session --cluster=$clusterID info --session=$sessionID | \
    while read param1 param2 param3; do
    if [ "$param1" = "session" ] || [ "$param1" = "last-active-at" ] || [ "$param1" = "hibernate" ]
    then
        echo -n "$param3 " >> $tmp;
    fi
    done;
    echo "" >> $tmp;
done
echo "$(date +"%F %T") pre session list :" >> $log
cat $tmp >> $log

let "i = 0";
cat $tmp | sort -r -k2 | \
while read param1 param2 param3; do
    let "i += 1";
#    echo "$(date +"%F %T") session $param1 ($param2)" >> $log
    if [ "$i" -gt 3 ]
    then 
    echo "$(date +"%F %T") terminate session $param1 ($param2)" >> $log
## Внимание! Эта строка удаляет сессии.
##    $spath/rac session --cluster=$clusterID terminate --session=$param1
    fi
done

echo "$(date +"%F %T") End" >> $log
echo "" >> $log
