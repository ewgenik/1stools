between() {
        vol="$1"
        min="$2"
        max="$3"
        if [ "$vol" -lt "$min" ]; then
                echo "$min"
        elif [ -n "$max" ] && [ "$vol" -gt "$max" ]; then
                echo "$max"
        else
                echo "$vol"
        fi
}


CPUCNT=$(nproc)

read -r _ kb _ < /proc/meminfo || exit 1
RAM=$((kb/1024))
#echo $RAM
#echo $((RAM/4))

cat << END_OF_CONF
max_connections = 100
shared_buffers = $((RAM/4))MB
effective_cache_size = $((RAM*3/4))MB
maintenance_work_mem = 1024MB
checkpoint_completion_target = 0.9
wal_buffers = 16MB
default_statistics_target = 100
random_page_cost = 1.1
effective_io_concurrency = 200
work_mem=512MB
min_wal_size = 2GB
max_wal_size = 4GB
max_worker_processes = 32
max_parallel_workers = 32

temp_buffers = 128MB
max_files_per_process = 10000
max_parallel_workers_per_gather = 0
max_parallel_maintenance_workers = $(between $((CPUCNT/4)) 2 6)
commit_delay = 1000
checkpoint_timeout = 15min
from_collapse_limit = 8
join_collapse_limit = 8
autovacuum_max_workers = $(between $((CPUCNT/2)) 2 999)
vacuum_cost_limit = $(between $((CPUCNT*50)) 200 99900)
autovacuum_naptime = 20s
autovacuum_vacuum_scale_factor = 0.01
autovacuum_analyze_scale_factor = 0.005
max_locks_per_transaction = 256
escape_string_warning = off
standard_conforming_strings = off
shared_preload_libraries = 'online_analyze, plantuner'
online_analyze.threshold = 50
online_analyze.scale_factor = 0.1
online_analyze.enable = on
online_analyze.verbose = off
online_analyze.min_interval = 10000
online_analyze.table_type = 'temporary'
plantuner.fix_empty_table = on
END_OF_CONF

