#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : Возвращает список соединений к серверу 1с
# Синтаксис  : getconnection.sh

# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

$spath/ras --daemon cluster
clusterID=$($spath/rac cluster list|grep cluster|sed 's/cluster                       : //')
echo clusterID:$clusterID

#$spath/rac connection --cluster=$clusterID list

# краткий вариант
 $spath/rac connection --cluster=$clusterID list | grep "host\|connected-at"
