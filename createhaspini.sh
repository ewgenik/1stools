#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : Создает файл nethasp.ini и все необходимые линки на него.
# Синтаксис  : createhaspini.sh

# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

echo "[NH_COMMON]
NH_TCPIP = Enabled

[NH_TCPIP]
NH_SERVER_ADDR = 192.168.0.1
NH_USE_BROADCAST = Disabled
" > /etc/nethasp.ini


mkdir -p $spath/conf
mkdir -p $spath/../conf
ln -sf /etc/nethasp.ini $spath/conf/nethasp.ini
ln -sf /etc/nethasp.ini $spath/../conf/nethasp.ini
ln -sf /etc/nethasp.ini /opt/nethasp.ini

