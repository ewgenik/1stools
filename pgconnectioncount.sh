#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : 	Тест производительности PostgreSQL сервера. 
# Синтаксис  : 	.\pgbenchmark.sh


# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

# создаем базу данных
PGPASSWORD="$SQL_P" psql --host localhost --username postgres --command 'select used, max_conn,res_for_super,max_conn-used-res_for_super res_for_normal from (select count(*) used from pg_stat_activity) t1, (select setting::int res_for_super from pg_settings where name=$$superuser_reserved_connections$$) t2, (select setting::int max_conn from pg_settings where name=$$max_connections$$) t3;'


