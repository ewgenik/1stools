#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : 	Создает 1С базу данных на localhost 1С сервере. 
#		Публикует созданую базу на localhost web сервере. 
# Синтаксис  : 	.\create1sbase.sh 1sbasename [sqlbasename]
# 		1sbasename - название базы 1С
# 		sqlbasename - название SQL базы данных, по умолчанию равно 1sbasename


#название базы 1С
basename=$1
#название SQL базы данных
refname=$2

# Проверка существования конфигурационного файла
CDIR=$(dirname "$0")
if [ ! -f "$CDIR/config.sh" ]; then
        echo "Файл конфигурации $CDIR/config.bkp не существует."
        exit 0
fi
. $CDIR"/config.sh"

#Настройки сервера
$spath/ras --daemon cluster
clusterID=$($spath/rac cluster list|grep cluster|sed 's/cluster                       : //')
echo clusterID:$clusterID

# IP адрес сервера
serverIP=$(hostname -I)
echo serverIP:$serverIP

# Строка состоящая из даты и времени для резервных копий конфигов
DATENAME=`date +%Y%m%d%H%M`

# проверяем введено ли название базы в качестве параметра 1
if [ ${#basename} -gt 3 ]; 
then 
	# проверяем введено ли название SQL базы в качестве параметра 2
	if [ ${#refname} -lt 3 ]; 
	then 
		# если название SQL базы не задано оно равно названию базы 1С
		refname=$basename;
	fi
	echo "1SBaseName=$basename";
	echo "SQLBaseName=$refname";

	# создаем базу 1С
	$spath/rac infobase --cluster=$clusterID create --create-database --name=$refname --dbms=PostgreSQL --db-server="localhost" --db-name=$refname --locale=ru --db-user="$SQL_U" --db-pwd="$SQL_P" --scheduled-jobs-deny=on --license-distribution=allow

else
	echo "Использование: $0 1sbasename [sqlbasename]"
	echo ""
	echo "Создает 1С базу данных на localhost 1С сервере. Публикует созданую базу на localhost web сервере."
	echo ""
	echo "Аргументы:"
	echo "1sbasename - название базы 1С (минимум 4 символа)"
	echo "sqlbasename - название SQL базы данных, по умолчанию равно 1sbasename"

fi
