#!/bin/bash

# ООО Гарант-Софт
# Никоноров Евгений
# Назначение : 	Создает сервис для 1С сервера.
# Синтаксис  : 	./setsrvservice.sh platformver
# 		platformver - версия платформы в формате 8.3.xx.yyyy, например 8.3.22.2177

#версия платформы
platformver=$1

# проверяем введена ли версия платформы
if [ ${#platformver} -gt 3 ]; 
then
    echo 123 
    systemctl link /opt/1cv8/x86_64/$platformver/srv1cv8-$platformver@.service
    systemctl enable /opt/1cv8/x86_64/$platformver/srv1cv8-$platformver@.service
    echo "Для запуска 1с сервера:
systemctl start srv1cv8-$platformver@default.service"
    echo "Для проверки состояния 1с сервера:
systemctl status srv1cv8-$platformver@default.service"
else
	echo "Использование: $0 platformver"
	echo ""
	echo "Создает сервис для 1С сервера."
	echo ""
	echo "Аргументы:"
	echo "platformver - версия платформы в формате 8.3.xx.yyyy, например 8.3.22.2177"
fi
